/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import conecciones.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kama
 */
public class EmpleadoDAO {
    private static final String SQL_SELECT = "SELECT * FROM empleado";
    private static final String SQL_SELECT_INDIVIDUAL = "SELECT idEmpleado, Nombre, AP, AM, Telefono FROM empleado WHERE idEmpleado=?";
    private static final String SQL_INSERT = "INSERT INTO empleado(Nombre, AP, AM, Telefono) VALUES(?, ?, ?, ?)";
    private static final String SQL_INSERT_ID = "INSERT INTO empleado(idEmpleado, Nombre, AP, AM, Telefono) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE empleado SET Nombre=?, AP=?, AM=?, Telefono=? WHERE idEmpleado=?";
    private static final String SQL_DELETE = "DELETE FROM empleado WHERE idEmpleado=?";
    private static final String SQL_AUTO_INCREMENT = "SELECT MAX(idEmpleado) FROM empleado";
    
    public static int siguiente_id() {
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_AUTO_INCREMENT);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }
        
        return id;
    }
    
     public static int NRegistros() {
        ArrayList<Empleado> empleados = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Empleado empleado = new Empleado(rs.getInt("idEmpleado"),
                        rs.getString("Nombre"),
                        rs.getString("AP"),
                        rs.getString("AM"),
                        (rs.getLong("Telefono")));
                empleados.add(empleado);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return empleados.size();
    }
     
     public static Empleado select_individual(String id) {
        Empleado empleado = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT_INDIVIDUAL);
            stmt.setString(1, id);//al orden de la instruccion de atributo
            rs = stmt.executeQuery();

            while (rs.next()) {
                empleado = new Empleado(rs.getInt("idEmpleado"),
                        rs.getString("Nombre"),
                        rs.getString("AP"),
                        rs.getString("AM"),
                        (rs.getLong("Telefono")));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return empleado;
    }
     
    public static ArrayList<Empleado> select() {
        ArrayList<Empleado> empleados = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Empleado empleado = new Empleado(rs.getInt("idEmpleado"),
                        rs.getString("Nombre"),
                        rs.getString("AP"),
                        rs.getString("AM"),
                        (rs.getLong("Telefono")));
                empleados.add(empleado);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return empleados;
    }

    public static int insert(Empleado empleado) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT);
            
            stmt.setString(1, empleado.getNombre());//al orden de la instruccion de atributo
            stmt.setString(2, empleado.getApellidoP());
            stmt.setString(3, empleado.getApellidoM());
            stmt.setLong(4, empleado.getTelefono());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int insert_id(Empleado empleado) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT_ID);
            
            stmt.setInt(1, empleado.getIdEmpleado());
            stmt.setString(2, empleado.getNombre());//al orden de la instruccion de atributo
            stmt.setString(3, empleado.getApellidoP());
            stmt.setString(4, empleado.getApellidoM());
            stmt.setLong(5, empleado.getTelefono());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int update(Empleado empleado) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_UPDATE);

            stmt.setString(1, empleado.getNombre());//al orden de la instruccion de atributo
            stmt.setString(2, empleado.getApellidoP());
            stmt.setString(3, empleado.getApellidoM());
            stmt.setLong(4, empleado.getTelefono());
            stmt.setInt(5, empleado.getIdEmpleado());

            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int delete(Empleado empleado) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_DELETE);
            stmt.setInt(1, empleado.getIdEmpleado());
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
}
