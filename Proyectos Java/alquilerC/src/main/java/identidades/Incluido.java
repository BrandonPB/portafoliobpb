/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import java.sql.Date;

/**
 *
 * @author Kama
 */
public class Incluido {
   private int idCoche;
   private int idAlquiler;
   private int cantidadDias;

    public Incluido() {
    }
    
    public Incluido(int idCoche, int idAlquiler, int cantidadDias) {
        this.idCoche = idCoche;
        this.idAlquiler = idAlquiler;
        this.cantidadDias = cantidadDias;
    }

    public Incluido(int idCoche, int idAlquiler) {
        this.idCoche = idCoche;
        this.idAlquiler = idAlquiler;
    }

    public int getIdCoche() {
        return idCoche;
    }

    public void setIdCoche(int idCoche) {
        this.idCoche = idCoche;
    }

    public int getIdAlquiler() {
        return idAlquiler;
    }

    public void setIdAlquiler(int idAlquiler) {
        this.idAlquiler = idAlquiler;
    }

    public int getCantidadDias() {
        return cantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        this.cantidadDias = cantidadDias;
    }

    @Override
    public String toString() {
        return "Incluido{" + "idCoche=" + idCoche + ", idAlquiler=" + idAlquiler + ", cantidadDias=" + cantidadDias + '}';
    }
   
}
