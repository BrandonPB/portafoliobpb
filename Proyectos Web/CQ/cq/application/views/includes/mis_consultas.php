<!-- testemonial Start -->
<section id="res"   class="testemonial">
			<div class="container">

				<div class="gallary-header text-center">
					<h2>
				Mis consultas
					</h2>
					<p>
				Ultimas consultas					</p>

				</div><!--/.gallery-header-->

				<div class="owl-carousel owl-theme" id="testemonial-carousel">

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src= "<?=base_url('assets/images/client/user.png')?>" alt="img"/>
                          
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							      DESCRIPCION D1		
							</p>
								<h3>
									<a href="#">
										Eleazar Gomez
									</a>
								</h3>
								<h4>TLAXCALA</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src=      "<?=base_url('assets/images/client/user.png')?>" alt="img"/>
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							  DESCRIPCION D2	
							</p>
								<h3>
									<a href="#">
									   Manuel Flores
									</a>
								</h3>
								<h4>TLAXCALA</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src=      "<?=base_url('assets/images/client/user.png')?>" alt="img"/>
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							     DESCRIPCION D3		
							</p>
								<h3>
									<a href="#">
									Jose Luis Robles
									</a>
								</h3>
								<h4>TLAXCALA</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src=      "<?=base_url('assets/images/client/user.png')?>" alt="img"/>
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
								     DESCRIPCION D4	
							   </p>
								<h3>
									<a href="#">
								Maria Teresa Gomez
									</a>
								</h3>
								<h4>PUEBLA</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src=      "<?=base_url('assets/images/client/user.png')?>" alt="img"/>
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
						          DESCRIPCION D5		
					     		</p>
								<h3>
									<a href="#">
										André-Pierre Gignac
									</a>
								</h3>
								<h4>TLAXCALA</h4>
							</div><!--/.home1-testm-txt-->	
						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

				

					<div class="home1-testm item">
						<div class="home1-testm-single text-center">
							<div class="home1-testm-img">
								<img src= "<?=base_url('assets/images/client/user.png')?>" alt="img"/>
							</div><!--/.home1-testm-img-->
							<div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
								<p>
							   DESCRIPCION D6		
							</p>
								<h3>
									<a href="#">
									Roberto Alvarado
									</a>
								</h3>
								<h4>TLAXCALA</h4>
							</div><!--/.home1-testm-txt-->	

						</div><!--/.home1-testm-single-->

					</div><!--/.item-->

				</div><!--/.testemonial-carousel-->
			</div><!--/.container-->

		</section><!--/.testimonial-->	
		<!-- testemonial End -->