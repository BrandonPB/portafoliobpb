<br><br><br><br><br><br>
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                  <br><br><br>

						
						
                        <div class="row">
							
							
							<div class="col-lg-10">
								<div class="card-box">
									<h4 class="m-t-0 header-title"><b>Inserte sus datos completos</b></h4>
									<p class="text-muted font-13 m-b-30">
	                                    Todos sus datos serán confidenciales
	                                </p>
	                                
									<form class="form-horizontal"  id="agregarss" role="form" method="post"  data-parsley-validate="" novalidate="">

										<div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Nombre</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Nombre" id="Nombre" placeholder="Brenda" data-parsley-id="17">
												<?=form_error('Nombre')?>
											</div>
										</div>

                                        <div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Apellido Paterno</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Apaterno" id="Apaterno" placeholder="Ortega" data-parsley-id="17">
												<?=form_error('Apaterno')?>
											</div>
										</div>
                                        
                                        <div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Apellido Materno</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Amaterno" id="Amaterno" placeholder="Mendez" data-parsley-id="17">
												<?=form_error('Amaterno')?>

											</div>
										</div>
                                       <br><br><br><br><br>     

                                        <div class="form-group3">
											<label for="inputEmail3" class="col-sm-4 control-label">Fecha de Nacimiento</label>
											<div class="col-sm-7">
												<input type="date" required="" parsley-type="date" class="form-control" name="Edad" id="Edad" data-parsley-id="17">
												<?=form_error('Edad')?>
											</div>
										</div>
                                        <br><br><br>  

										<div class="form-group3">
											<label for="inputEmail3" class="col-sm-4 control-label">Telefono</label>
											<div class="col-sm-7">
												<input type="text" required="" parsley-type="text"  placeholder="2461234567" class="form-control" name="Telefono" id="Telefono" data-parsley-id="17">
												<?=form_error('Telefono')?>
											</div>
										</div>
                                        <br><br><br> 
										<div class="caja">
											<select name="Estado" id="Estado">
												<option disabled="disabled"  value="SEL">--Seleccionar Estado--</option>
												<!--PONER ALARTA -->
												<option value="EDM">Estado de México</option>
												<option value="AGS">Aguascalientes</option>
												<option value="BCN">Baja California</option>
												<option value="BCS">Baja California Sur</option>
												<option value="CAM">Campeche</option>
												<option value="CHP">Chiapas</option>
												<option value="CHI">Chihuahua</option>
												<option value="COA">Coahuila</option>
												<option value="COL">Colima</option>
												<option value="DUR">Durango</option>
												<option value="GTO">Guanajuato</option>
												<option value="GRO">Guerrero</option>
												<option value="HGO">Hidalgo</option>
												<option value="JAL">Jalisco</option>
												<option value="MEX">Ciudad de México</option>
												<option value="MIC">Michoacán</option>
												<option value="MOR">Morelos</option>
												<option value="NAY">Nayarit</option>
												<option value="NLE">Nuevo León</option>
												<option value="OAX">Oaxaca</option>
												<option value="PUE">Puebla</option>
												<option value="QRO">Querétaro</option>
												<option value="ROO">Quintana Roo</option>
												<option value="SLP">San Luis Potosí</option>
												<option value="SIN">Sinaloa</option>
												<option value="SON">Sonora</option>
												<option value="TAB">Tabasco</option>
												<option value="TAM">Tamaulipas</option>
												<option value="TLX">Tlaxcala</option>
												<option value="VER">Veracruz</option>
												<option value="YUC">Yucatán</option>
												<option value="ZAC">Zacatecas</option>

											</select>
										</div>

										<div class="caja">
											<select name="Sexo" id="Sexo">
											<option disabled="disabled"  value="M">--Seleccionar sexo--</option>
												<option  value="M">M</option>
												<option value="F">F</option>
											</select>
										</div>

                                        <div class="form-group4">
											<label for="inputEmail3" class="col-sm-4 control-label">Correo</label>
											<div class="col-sm-7">
												<input type="email" required="" parsley-type="text" class="form-control" name="Correo" id="Correo"   placeholder="correo@email.com" data-parsley-id="17">
												<?=form_error('Usuario')?>
											</div>
										</div>
                                        <br><br><br>  

										<div class="form-group5">
											<label for="hori-pass1" class="col-sm-3 control-label">Password*</label>
											<div class="col-sm-2">
												<input id="hori-pass1" type="password" placeholder="Password" required="" name="Password" class="form-control" id="password" data-parsley-id="19">
												<?=form_error('Password')?>
											</div>
										</div>


										<div class="form-group5">
											<label for="hori-pass2" class="col-sm-3 control-label">Confirm Password *</label>
											<div class="col-sm-2">
												<input data-parsley-equalto="#hori-pass1" type="password" required="" name="password1" placeholder="Password" class="form-control" id="password1" data-parsley-id="21">
											</div>
										</div>

										
							
										
                                        <br><br><br>  
										<div class="form-group6">
											<div class="col-sm-offset-4 col-sm-8">
                                                <div class="checkbox">
                                                    <input id="remember-2" type="checkbox" data-parsley-multiple="remember-2" data-parsley-id="26">
                                                    <label for="remember-2">Recuérdame</label>
                                                </div>
											</div>
										</div>
                                        <br><br><br>  
										<div class="form-group">
											<div class="col-sm-offset-5 col-sm-8">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Registrar

												</button>
												
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
										
                        
                        

    
    
     

            		</div> <!-- container -->
                               
                </div> <!-- content -->
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


            </div>


		
				
			<script> 
			var Aalerta="<?php echo $mensaje;?>";
			</script>

		

