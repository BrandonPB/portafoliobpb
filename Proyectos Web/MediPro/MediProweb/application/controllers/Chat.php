<?php


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends CI_Controller {
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('chatmodel');
	}
	



	public function index($nickusuario){
		$data['persona']=$this->Mpaciente->obtener_dato($this->session->Nombre);
		$this->load->view('chat_vista',$data);
	}

	
	public function mensajes_canal(){
		$usuario = $this->input->post('usuario');
		$mensajesCanal = $this->chatmodel->getMensajesCanalOnTime($usuario);
		echo json_encode($mensajesCanal);
	}
	
	public function add_mensaje_canal(){
		$mensaje = $this->input->post('mensaje');
		$usuario = $this->input->post('usuario');
		$this->chatmodel->addMensaje($usuario,$mensaje);
		$data['fecha'] = date('Y-m-d H:i:s');
		$data['usuario'] = $usuario;
		$data['mensaje'] = $mensaje;
		echo json_encode($data);
		
	}
}

